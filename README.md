# Prova de Front-end para a MaxMilhas
---

## Instala��o do projeto

### Rodando o projeto com Docker

Criei uma configura��o do Docker para facilitar a avalia��o do projeto, para rodar o projeto usando Docker siga os passoa abaixo:

**Primeiro passo** - Fa�a o download dos arquivos via git, execute o comando no terminal `git clone https://juniotsl@bitbucket.org/juniotsl/max.git`

**Segundo passo** - Execute o comando `cd max`

**Terceiro passo** - Execute o comando `docker build -t max .` 

**Quarto passo** - Ap�s terminar de rodar o comando execute `docker run -it -v ${PWD}:/usr/src/app -v /usr/src/app/node_modules  -p 3000:3000 --rm max`

**Quinto passo** - Acessar o seu navagador no endere�o http://localhost:3000 ( Lembrando que se tiver outro projeto rodando na porta :3000 voc� devera encera-lo)

### Rodando o projeto com npm

Para rodar o projeto usando o NPM voc� devera seguir os seguintes passos:

**Primeiro passo** - Fa�a o download dos arquivos via git, execute o comando no terminal `git clone https://juniotsl@bitbucket.org/juniotsl/max.git`

**Segundo passo** - Execute o comando `cd max`

**Terceiro passo** - Executar o comando `npm install`

**Quarto passo** - Executar o comando `npm start`

**Quinto passo** - Acessar o seu navagador no endere�o http://localhost:3000 ( Lembrando que se tiver outro projeto rodando na porta :3000 voc� devera encera-lo)

--- 

## Como o projeto foi desenvolvido

Inicialmente eu foquei em desenvolver o projeto pensando em **Mobile First** por isso n�o sobrou tempo para desenvolver a vers�o 
desktop do projeto, basicamente o projeto segue as dicas de API que foram dadas, usando axios vou no endPoint e resgato os dados
de uma viagem seguindo as informa��o adicionadas pelo usu�rio, usando Promise fa�o com que a busca fique ass�ncrona na parte que faz o put
e resolve todas com Resolve.all quando j� tem os dados. 

O projeto foi desenvolvido usando: 

 - React
 - Bootstrap
 - Axios
 - MomentJs ( para as formata��es de data) 
 - react-datepicker ( para o calendario) 
 - node-sass ( Pre-processador de css utilizado no projeto) 
 
---

## Melhorias que gostaria de ter feito no projeto

Gostaria de ter escrito alguns teste unitarios para o projeto , ter utilizado um gerenciado de estado para melhorar a
comuni��o entre os componentes , ter desenvolvido a vers�o desktop do projeto e usado um redux-form por exemplo para auxiliar 
na cria��o/manuten��o dos formularios do projeto. 