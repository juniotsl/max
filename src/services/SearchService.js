import API from './Api';
/**
 * SearchService
 * Serviço necessario para fazer uma busca
 * @param {*} data 
 */
export async function SearchService(data)  {
    let inbound = [];
    let outbound = [];

    return new Promise((resolve , reject) => {
        postSearch(data) //Faço a primeira busca, retornando as airlines que tem voo nessa data
            .then(res => {
                const airlines = res.airlines;
                const idSearch = res.id;
                Promise.all(airlines.map(promise => {
                    if(promise.status.enable) {
                        return getSearch(idSearch , promise.label)
                        .then((singleAirline) => {
                            if(singleAirline.inbound.length > 0) { // Verifico se tem alguma viagem de ida para essa companhia 
                                inbound.push(...singleAirline.inbound);
                            }
                            if(singleAirline.outbound.length > 0) { // Verifico se tem alguma viagem de volta para essa companhia 
                                outbound.push(...singleAirline.outbound);
                            }
                            
                        })
                        .catch((err) => console.error(err))
                    }
                    return true
                })) //Faço a busca por todas as airlines que retornaram true, e espero buscar todas
                .then(() => {
                   resolve({outbound , inbound }); // Resolvo a minha promise retornando um array com todas as datas de saida e volta
                });
            })
            .catch(error => reject(error))
    });

}

/**
 * PostSearch
 * Função responsavel em iniciar a busca de uma viagem
 * @param {*} data 
 */
function postSearch(data) {

    return new Promise((resolve , reject) => {
        API.post(`/search?time=${Date.now()}`, data)
        .then(res => {
            resolve(res.data)
        })
        .catch((error) => {
            console.error("[ERROR]:" + error);
            reject(error)
        })
    })
}

/**
 * getSearch
 * Função responsavel por buscar a viagem por uma airline
 * @param {*} searchID 
 * @param {*} airline 
 */
function getSearch(searchID , airline) {
    return new Promise((resolve , reject) => {
        API.get(`/search/${searchID}/flights?airline=${airline}`)
        .then(res => {
            resolve(res.data)
        })
        .catch((error) => {
            reject(error)
        })
    })
}