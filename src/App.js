import React, { Component } from 'react';
import './App.scss';
import Header from './components/header/Header';
import Search from './components/search/Search';

class App extends Component {
  
  render() {
    return (
      <React.Fragment>

        <Header />

        <Search />

      </React.Fragment>
    );
  }
}

export default App;
