import React, { Component } from 'react';
import './Header.scss';

class Header extends Component {
  
  render() {
    return (
        <header>
            <div className="container-fluid no-padding">
                <div className="menu-group--primary">
                    <button className="menu-button">
                        <div className="line"></div>
                        <div className="line"></div>
                        <div className="line"></div>
                    </button>
                    <span className="menu-logo">Teste Front</span>
                </div>
                <div className="menu-group--secundary">
                    <span className="menu-timer"> <i className="far fa-clock"></i>  20:00 </span>
                </div>
            </div>
        </header>
    );
  }
}

export default Header;
