import React, { Component } from 'react';
import './Results.scss';
import Moment from 'moment';

class Results extends Component {

    createList = (value) => {
        Moment.locale('pt-BR');
        let table = []

        for (let i = 0; i < value.length; i++) {
            let price = 0;
            let desconto = 0;
            const bestPrice = value[i]['pricing']['bestPriceAt'];

            price = value[i]['pricing'][bestPrice]['saleTotal'];

            if(bestPrice === 'miles') {
                let companyPrice = value[i]['pricing']['airline']['saleTotal'];
                desconto = ((companyPrice - price) * 100) / companyPrice ;
            }

            table.push(
                <li key={value[i]['id']}>
                    <div className="headerFly">
                        <div className="">
                            <b>{value[i]['airline']}</b>
                            <span>{value[i]['flightNumber']}</span>
                        </div>
                        <div className="">
                            <b>{Moment(value[i]['departureDate']).format('HH:mm')}</b>
                            <span>{value[i]['to']}</span>
                        </div>
                        <div className="">
                            <b>{value[i]['duration']} min</b>
                            <span>{parseInt(value[i]['stops']) > 0 ? `${value[i]['stops']} PARADAS` : 'VOO DIRETO' }</span>
                        </div>
                        <div className="">
                            <b>{Moment(value[i]['arrivalDate']).format('HH:mm')}</b>
                            <span>{value[i]['from']}</span>
                        </div>
                    </div>
                    {bestPrice === 'miles' &&
                        <span className="price-company">{value[i]['pricing']['airlineName']} <b>R$ {value[i]['pricing']['airline']['saleTotal'].toFixed(2)}</b></span>
                    }
                    <button>R$ {price.toFixed(2)}</button>
                    {bestPrice === 'airline' && 
                        <span className="total-desconto">Menor preço na cia Aérea</span>
                    }
                    {bestPrice === 'miles' &&
                        <span className="total-desconto">Economize {(desconto).toFixed(2)}% na maxmilhas</span>
                    }

                    <div className="more-information"> <b>+</b> Detalhes do voo</div>
                </li>
          )
        }
        return table
    }

    render() {
        return (
            <React.Fragment>
                <div className="alarm">
                    <span>Crie um alarme  para essa busca</span>
                    <i className="far fa-bell"></i>
                </div>

                <div className="result-search">
                    <div className="header-result">
                        <span>Cia Aérea</span>
                        <span>Partida</span>
                        <span>Duração</span>
                        <span>Chegada</span>
                    </div>

                    {this.props.showInbound &&
                        <ul>
                            {this.createList(this.props.result['inbound'])}
                        </ul>
                    }
                    {!this.props.showInbound &&
                        <ul>
                            {this.createList(this.props.result['outbound'])}
                        </ul>
                    }
                    
                </div>
            </React.Fragment>
        );
    }
}

export default Results;
