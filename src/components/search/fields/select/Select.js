import React, { Component } from 'react';
import './Select.scss';

class FieldSelect extends Component {
    constructor(props) {
        super(props);
        this.state = {
            startValue: this.props.startValue
        }
    }


    handleChange = e => {
        this.props.onChange(this.props.id , e.target.value);
        this.setState({
            startValue: e.target.value
        })
    }

    createTable = () => {
        let table = []
        
        const start = this.props.startSelect; 
        const end = this.props.endSelect;
        
        for (let i = start; i <= end; i++) {
          table.push(<option key={i} value={i}>{i}</option>)
        }
        return table
    }

    render() {
        return (
            <React.Fragment>
                <div className="wrap-field field-passenger ">
                    <label>{this.props.label}</label>
                    <select value={this.state.startValue} onChange={this.handleChange}>
                        {this.createTable()}
                        
                    </select>
                    <i className="fas fa-chevron-down arrow-icon"></i>
                </div>
            </React.Fragment>
        );
    }
}

export default FieldSelect;
