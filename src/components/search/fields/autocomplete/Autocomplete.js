import React, { Component } from "react";
import './Autocomplete.scss';
import * as data from '../../../../services/airports.json';


class Autocomplete extends Component {


  static defaultProps = {
    suggestions: []
  };

  constructor(props) {
    super(props);

    this.state = {
      // The active selection's index
      activeSuggestion: 0,
      // The suggestions that match the user's input
      filteredSuggestions: [],
      // Whether or not the suggestion list is shown
      showSuggestions: false,
      // What the user has entered
      userInput: ""
    };
  }

  

  __search = () => {
    const suggestions = data.airports;
    let code = "";
    if(this.state.userInput) {
        Object.keys(suggestions).reduce((r , key) => {
            return suggestions[key].reduce((k) => {
                if(k && k.toLowerCase().indexOf(this.state.userInput.toLowerCase()) > -1 ){
                    code = key;
                }
                return key;
            })
        })
        this.props.onChange(code);
    }
  }

  onChange = e => {
    const suggestions = data.airports;
    const userInput = e.currentTarget.value;
    let filter = [];

    if(userInput.length > 3) {
        
        Object.keys(suggestions).reduce((r , key) => {
            return suggestions[key].reduce((k) => {
                if(k && k.toLowerCase().indexOf(userInput.toLowerCase()) > -1 ){
                    filter.push(k);
                }
                return key
            })
        })
    }
    

    this.setState({
      activeSuggestion: 0,
      showSuggestions: true,
      filteredSuggestions: filter,
      userInput: e.currentTarget.value
    });
  };

  onClick = e => {
    this.setState({
      activeSuggestion: 0,
      filteredSuggestions: [],
      showSuggestions: false,
      userInput: e.currentTarget.innerText
    } , () => {this.__search()});
  };

  onKeyDown = e => {
    const { activeSuggestion, filteredSuggestions } = this.state;

    // User pressed the enter key
    if (e.keyCode === 13) {
      this.__search();
    }
    // User pressed the up arrow
    else if (e.keyCode === 38) {
      if (activeSuggestion === 0) {
        return;
      }

      this.setState({ activeSuggestion: activeSuggestion - 1 });
    }
    // User pressed the down arrow
    else if (e.keyCode === 40) {
      if (activeSuggestion - 1 === filteredSuggestions.length) {
        return;
      }

      this.setState({ activeSuggestion: activeSuggestion + 1 });
    }
  };

  render() {
    const {
      onChange,
      onClick,
      onKeyDown,
      state: {
        activeSuggestion,
        filteredSuggestions,
        showSuggestions,
        userInput
      }
    } = this;

    let suggestionsListComponent;

    if (showSuggestions && userInput) {
      if (filteredSuggestions.length) {
        suggestionsListComponent = (
          <ul className="suggestions">
            {filteredSuggestions.map((suggestion, index) => {
              let className;

              // Flag the active suggestion with a class
              if (index === activeSuggestion) {
                className = "suggestion-active";
              }

              return (
                <li className={className} key={suggestion} onClick={onClick}>
                  {suggestion}
                </li>
              );
            })}
          </ul>
        );
      }
    }

    

    return (
      <React.Fragment>
        <div className="wrapField">
          <input
            type="text"
            onChange={onChange}
            onKeyDown={onKeyDown}
            value={userInput}
            placeholder="Pesquisar"
          />
        </div>
        {suggestionsListComponent}
      </React.Fragment>
    );
  }
}


export default Autocomplete;
