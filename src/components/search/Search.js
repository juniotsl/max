import React, { Component } from 'react';
import {SearchService} from '../../services/SearchService';
import './Search.scss';
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";
import FieldSelect from './fields/select/Select';
import Autocomplete from './fields/autocomplete/Autocomplete';
import Results from './results/Results';
import Moment from 'moment';


class Search extends Component {
  
    constructor(props) {
        super(props);

        let endDate = new Date();
        endDate.setDate(endDate.getDate() + 5);

        this.state = {
            startDate: new Date(),
            endDate: endDate,
            showPickCabin: false,
            adults:1,
            children:0,
            baby:0,
            calcPassagers: 1,
            cabin:"EC",
            loadingSearch: false,
            resumeSearch: false,
            showInbound: false,
            showError: false
        };
    }

    handleChangeStart = (date) => this.setState({startDate: date});

    handleChangeEnd = (date) => this.setState({endDate: date});

    changeValueCabin = e => this.setState({cabin: e.target.value})

    toggleBoxCabin = () => this.setState({showPickCabin: !this.state.showPickCabin})

    toggleInbound = () => this.setState({showInbound: !this.state.showInbound})

    toggleResume = () => this.setState({resumeSearch: !this.state.resumeSearch})

    changeCompanyFrom = (value) => this.setState({from: value });

    changeCompanyTo = (value) => this.setState({to: value});

    hideError = () => this.setState({showError: false})

    changeValueSelect = (label , value) => {
      this.setState({
        [label] : value
      } , () => this.setState({
        calcPassagers: parseInt(this.state.adults) + parseInt(this.state.children) + parseInt(this.state.baby)
      }))
    }

    submitForm = (e) => {
        e.preventDefault();
        if(this.state.to && this.state.from){
            this.setState({loadingSearch: true})

            const data = {
                tripType: "RT",
                from: this.state.from, //origem
                to: this.state.to, //destino
                outboundDate: Moment(this.state.startDate).format('YYYY-MM-DD'), //data de partida
                inboundDate: Moment(this.state.endDate).format('YYYY-MM-DD'), //data de volta
                cabin: this.state.cabin, //classe econômica (EC) ou executiva (EX)
                adults: this.state.adults, //adultos
                children: this.state.children, //crianças
                infants: this.state.baby //bebês
            }
            
            SearchService(data)
            .then(res => {
                console.log(res);
                if(res['inbound'].length > 0 || res['outbound'].length > 0 ) {
                    this.setState({
                        resultSearch: res,
                        resumeSearch:true,
                        showError: false,
                        loadingSearch: false
                    });
                }  else {
                    this.setState({
                        resultSearch: res,
                        resumeSearch:false,
                        showError: true,
                        loadingSearch: false
                    });
                }
                
            });
        }
    }
  

  render() {
    Moment.locale('pt-BR');
   
    return (
       <React.Fragment>
        {(this.state.resumeSearch && !this.state.loadingSearch) &&
            <React.Fragment>
                <section className="resume-search" onClick={this.toggleResume} >
                    <div className="row wrap-fields">
                        <div className="col-12">
                            <span className="resume-field resume-airport"><i className="fas fa-map-marker-alt icon-input"></i> {this.state.from} - {this.state.to}</span>
                            <span className="resume-field resume-data "><i className="far fa-calendar-alt icon-input"></i> {Moment(this.state.startDate).format('D MMM YYYY')} </span>
                            {this.state.endDate &&
                                <span className="resume-field resume-data"><i className="far fa-calendar-alt icon-input"></i> {Moment(this.state.endDate).format('D MMM YYYY')}</span>
                            }
                            <span className="resume-field resume-cabin"><i className="fas fa-user-friends icon-fake-input"></i>{this.state.calcPassagers}</span>
                        </div>
                    </div>
                </section>  

                            
                <div className="wrap-outbound-inbound">
                    <div className={!this.state.showInbound ? 'item active' : 'item' } onClick={this.toggleInbound}><span>Selecione sua ida</span></div>
                    <div className={this.state.showInbound ? 'item active' : 'item'} onClick={this.toggleInbound}><span>Selecione sua volta</span></div>
                </div>

                <Results result={this.state.resultSearch} showInbound={this.state.showInbound} />            

            </React.Fragment>
        }

        {(!this.state.resumeSearch && !this.state.loadingSearch && !this.state.showError )  &&
            <section className="search-form">
                <form id="searchForm" method="GET" onSubmit={this.submitForm}>
                    <div className="row">
                        <div className="col-12 ">
                            <div className="wrap-field field-outboundDate">
                                <label>Sair de</label>
                                <Autocomplete 
                                  onChange={this.changeCompanyFrom}
                                />
                                <i className="fas fa-map-marker-alt icon-input"></i>
                            </div>
                            
                        </div>
                        
                        <div className="col-12 ">
                            <div className="wrap-field field-inboundDate">
                                <label>Ir para</label>
                                <Autocomplete 
                                onChange={this.changeCompanyTo}
                                />
                                <i className="fas fa-map-marker-alt icon-input"></i>
                            </div>
                        </div>

                        <div className="col-6 ">
                            <div className="wrap-field field-startDate">
                                <label>Data da ida</label>
                                <DatePicker
                                    selected={this.state.startDate}
                                    selectsStart
                                    minDate={new Date()}
                                    startDate={this.state.startDate}
                                    endDate={this.state.endDate}
                                    onChange={this.handleChangeStart}
                                    dateFormat="dd/MM/yyyy"
                                />
                                <i className="far fa-calendar-alt icon-input"></i>
                            </div>
                        </div>

                        <div className="col-6 ">
                            <div className="wrap-field field-endDate">
                                <label>Data da volta</label>
                                <DatePicker
                                    selected={this.state.endDate}
                                    selectsEnd
                                    minDate={new Date()}
                                    startDate={this.state.startDate}
                                    endDate={this.state.endDate}
                                    onChange={this.handleChangeEnd}
                                    dateFormat="dd/MM/yyyy"
                                    placeholderText="Selecione um dia para voltar"
                                />
                                <i className="far fa-calendar-alt icon-input"></i>
                            </div>
                        </div>

                        <div className="col-12 wrap-passengers">
                            <span className="title-passengers">Passageiros e classe de voo</span>

                            <div className="container-passengers" onClick={this.toggleBoxCabin}>
                                <span className="wrap-passengers--number">{ this.state.calcPassagers  }</span>
                                <div className="wrap-passengers--type">
                                    <span>{this.state.calcPassagers > 1 && (this.state.baby > 0 || this.state.children > 0) ? 'Passageiros' : this.state.adults > 1 ? 'Adultos' : 'Adulto'}</span>
                                    <span>{this.state.cabin === 'EC' ? 'Classe econômica' : 'Classe executiva' }</span>
                                </div>
                                <i className="fas fa-user-friends icon-fake-input"></i>
                            </div>

                            {this.state.showPickCabin  &&
                            <div className="select-passengers">
                                
                                <FieldSelect 
                                label="ADULTOS"
                                id="adults"
                                startValue={this.state.adults}
                                onChange={this.changeValueSelect}
                                startSelect={1}
                                endSelect={10}
                                />

                                <FieldSelect 
                                label="CRIANÇAS"
                                id="children"
                                startValue={this.state.children}
                                onChange={this.changeValueSelect}
                                startSelect={0}
                                endSelect={this.state.adults}
                                />

                                <FieldSelect 
                                label="BEBÊS"
                                id="baby"
                                startValue={this.state.baby}
                                onChange={this.changeValueSelect}
                                startSelect={0}
                                endSelect={this.state.adults}
                                />

                                <div className="wrap-field field-passenger field-cabin ">
                                    <label>Classe de voo</label>
                                    <select value={this.state.cabin} onChange={this.changeValueCabin}>
                                        <option value="EC" >Classe econômica</option>
                                        <option value="EX" >Classe executiva</option>
                                    </select>
                                    <i className="fas fa-chevron-down arrow-icon"></i>
                                </div>
                            </div>
                            }
                            
                        </div>
                        
                        <div className="col-12">
                            <button className="button-form">Pesquisar passagem</button>
                        </div>
                    </div>
                </form>
            </section>
        }


        {this.state.loadingSearch &&
          <div className="loading">
            <div className="spinner-border text-primary" role="status">
              <span className="sr-only">Loading...</span>
            </div>
            <span>Quase lá! Estamos procurando os melhores voos para você.</span>
          </div>
        }

        {(!this.state.loadingSearch && this.state.showError) &&
          <div className="error">
            <span className="title">Ah não!!</span>
            <span className="msg">Não temos voos disponíveis para sua busca</span>
            <button onClick={this.hideError}>Pesquisar Novamente</button>
          </div>
        }


       </React.Fragment>                 
    );
  }
}

export default Search;
